FROM node:18-alpine
COPY . /app
WORKDIR /app
CMD ["node","index.js"]
EXPOSE 5000
