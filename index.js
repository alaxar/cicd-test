const express = require("express")
const app = express();

app.get("/", function(req, res) {
	res.send("This is home page testing ci/cd pipeline");
});

app.get('/about', function(req, res) {
	res.send("This is about page");
});

app.get('/contact', function(req, res) {
	res.send("this is contact page for luel");
});


app.listen(5000, function() {
	console.log("Waiting for clients to connect...");
});
